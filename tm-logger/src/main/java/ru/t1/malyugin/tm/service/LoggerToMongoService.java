package ru.t1.malyugin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Primary
@Component("loggerToMongoService")
public final class LoggerToMongoService implements ILoggerService {

    @NotNull
    private final static String MONGO_DATABASE_NAME = "TM_LOG";

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient;

    @Autowired
    public LoggerToMongoService(@NotNull final MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final MongoDatabase mongoDatabase = mongoClient.getDatabase(MONGO_DATABASE_NAME);
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}