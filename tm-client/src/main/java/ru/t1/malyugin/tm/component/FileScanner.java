package ru.t1.malyugin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.api.service.ICommandService;
import ru.t1.malyugin.tm.api.service.ILoggerService;
import ru.t1.malyugin.tm.api.service.IPropertyService;
import ru.t1.malyugin.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final ICommandService commandService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ILoggerService loggerService;

    @Autowired
    public FileScanner(
            @NotNull final ICommandService commandService,
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap,
            @NotNull final ILoggerService loggerService
    ) {
        this.commandService = commandService;
        this.propertyService = propertyService;
        this.bootstrap = bootstrap;
        this.loggerService = loggerService;
    }

    public void start() {
        @NotNull final Iterable<AbstractCommand> commands = commandService.getCommands();
        commands.forEach(command -> this.commands.add(command.getName()));
        executorService.scheduleWithFixedDelay(this::scan, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    private void scan() {
        @NotNull final String commandDir = propertyService.getApplicationCommandScannerDir();

        @NotNull final Set<File> files = Stream.of(new File(commandDir).listFiles())
                .filter(f -> !f.isDirectory())
                .collect(Collectors.toSet());

        for (@NotNull final File file : files) {
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            try {
                if (check) {
                    bootstrap.processCommand(fileName);
                    Files.deleteIfExists(Paths.get(commandDir + fileName));
                }
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
            }
        }
    }

}