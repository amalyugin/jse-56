package ru.t1.malyugin.tm.command.scheme;

import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.command.AbstractCommand;
import ru.t1.malyugin.tm.enumerated.Role;

public abstract class AbstractSchemeCommand extends AbstractCommand {

    @NotNull
    @Override
    public Role[] getRoles() {
        return ArrayUtils.toArray();
    }

}