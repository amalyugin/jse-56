package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.malyugin.tm.command.AbstractCommand;

@Component
public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String DESCRIPTION = "Show command list";

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (@NotNull final AbstractCommand command : commandService.getCommands()) System.out.println(command);
    }

}