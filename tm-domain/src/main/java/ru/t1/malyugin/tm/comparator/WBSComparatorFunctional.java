package ru.t1.malyugin.tm.comparator;

import ru.t1.malyugin.tm.api.model.IWBS;

import java.util.Comparator;

public enum WBSComparatorFunctional implements Comparator<IWBS> {

    CREATED {
        @Override
        public int compare(final IWBS o1, final IWBS o2) {
            if (o1 == null || o2 == null) return 0;
            return o1.getCreated().compareTo(o2.getCreated());
        }
    },

    NAME {
        @Override
        public int compare(final IWBS o1, final IWBS o2) {
            if (o1 == null || o2 == null) return 0;
            return o1.getName().compareTo(o2.getName());
        }
    },

    STATUS {
        @Override
        public int compare(final IWBS o1, final IWBS o2) {
            if (o1 == null || o2 == null) return 0;
            return o1.getStatus().getDisplayName().compareTo(o2.getStatus().getDisplayName());
        }
    }

}
