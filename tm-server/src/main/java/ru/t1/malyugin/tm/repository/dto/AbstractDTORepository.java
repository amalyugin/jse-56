package ru.t1.malyugin.tm.repository.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.dto.IDTORepository;
import ru.t1.malyugin.tm.dto.model.AbstractDTOModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Repository
@NoArgsConstructor
@Scope("prototype")
public abstract class AbstractDTORepository<M extends AbstractDTOModel> implements IDTORepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getClazz();

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> modelList) {
        modelList.forEach(this::add);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void set(@NotNull final Collection<M> modelList) {
        clear();
        modelList.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull M model) {
        entityManager.remove(model);
    }


}