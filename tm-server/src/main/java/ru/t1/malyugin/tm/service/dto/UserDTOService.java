package ru.t1.malyugin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.repository.dto.IUserDTORepository;
import ru.t1.malyugin.tm.api.service.dto.IUserDTOService;
import ru.t1.malyugin.tm.api.service.property.IPropertyService;
import ru.t1.malyugin.tm.dto.model.UserDTO;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    protected IUserDTORepository getRepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String pass,
            @Nullable final String email,
            @Nullable final Role role
    ) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(pass)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (!StringUtils.isBlank(email) && isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, pass);
        if (passwordHash == null) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        if (role != null) user.setRole(role);
        add(user);
        return user;
    }

    @Override
    public void lockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    public void unlockUser(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final UserDTO userDTO = findOneByLogin(login);
        if (userDTO == null) return;
        remove(userDTO);
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @Nullable final UserDTO userDTO = findOneByEmail(email);
        if (userDTO == null) return;
        remove(userDTO);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        update(user);
    }

    @Override
    public void update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        update(user);
    }

    @Nullable
    @Override
    public UserDTO findOneByEmail(@Nullable final String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneByLogin(@Nullable final String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @NotNull final IUserDTORepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable String email) {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        return findOneByEmail(email) != null;
    }

    @Override
    public boolean isLoginExist(@Nullable String login) {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        return findOneByLogin(login) != null;
    }

}