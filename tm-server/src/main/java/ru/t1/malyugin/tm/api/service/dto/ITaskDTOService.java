package ru.t1.malyugin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IWBSDTOService<TaskDTO> {

    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String taskId,
            @Nullable String projectId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String taskId,
            @Nullable String projectId
    );

}