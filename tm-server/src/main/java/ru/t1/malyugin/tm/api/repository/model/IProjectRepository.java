package ru.t1.malyugin.tm.api.repository.model;

import ru.t1.malyugin.tm.model.Project;

public interface IProjectRepository extends IWBSRepository<Project> {

}