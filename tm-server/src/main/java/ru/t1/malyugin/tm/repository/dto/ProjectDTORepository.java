package ru.t1.malyugin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDTORepository extends AbstractWBSDTORepository<ProjectDTO> implements IProjectDTORepository {

    @NotNull
    @Override
    public Class<ProjectDTO> getClazz() {
        return ProjectDTO.class;
    }

}