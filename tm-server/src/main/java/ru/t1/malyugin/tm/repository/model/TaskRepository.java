package ru.t1.malyugin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.model.ITaskRepository;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskRepository extends AbstractWBSRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Class<Task> getClazz() {
        return Task.class;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.user.id = :userId AND m.project.id = :projectId";
        entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}