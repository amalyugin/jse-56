package ru.t1.malyugin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    @NotNull
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull String id);

    long getSize();

    void update(@NotNull M model);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    @NotNull
    EntityManager getEntityManager();


}