package ru.t1.malyugin.tm.endpoint;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.malyugin.tm.api.service.IAuthService;
import ru.t1.malyugin.tm.dto.model.SessionDTO;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.user.AccessDeniedException;
import ru.t1.malyugin.tm.exception.user.PermissionException;

public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    protected IAuthService authService;

    @NotNull
    protected SessionDTO checkPermission(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @NotNull final SessionDTO session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected SessionDTO checkPermission(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (StringUtils.isBlank(token)) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}