package ru.t1.malyugin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.malyugin.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.malyugin.tm.dto.model.SessionDTO;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    @NotNull
    @Override
    public Class<SessionDTO> getClazz() {
        return SessionDTO.class;
    }

}