package ru.t1.malyugin.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}