package ru.t1.malyugin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.malyugin.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.malyugin.tm.api.service.dto.IProjectDTOService;
import ru.t1.malyugin.tm.dto.model.ProjectDTO;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.NameEmptyException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;

@Service
@NoArgsConstructor
public final class ProjectDTOService extends AbstractWBSDTOService<ProjectDTO> implements IProjectDTOService {

    @NotNull
    @Override
    protected IProjectDTORepository getRepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @Override
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        add(userId, project);
        return project;
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        update(userId, project);
    }
}