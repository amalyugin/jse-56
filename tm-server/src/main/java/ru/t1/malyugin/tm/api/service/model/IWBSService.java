package ru.t1.malyugin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.AbstractWBSModel;

import java.util.Comparator;
import java.util.List;

public interface IWBSService<M extends AbstractWBSModel> extends IUserOwnedService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    @SuppressWarnings("rawtypes")
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator);

    void changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}
